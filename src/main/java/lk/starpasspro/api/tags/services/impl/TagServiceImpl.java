package lk.starpasspro.api.tags.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lk.starpasspro.api.tags.dtos.TagInputDto;
import lk.starpasspro.api.tags.dtos.TagOutputDto;
import lk.starpasspro.api.tags.mappers.TagMapper;
import lk.starpasspro.api.tags.models.Tag;
import lk.starpasspro.api.tags.repositories.TagRepository;
import lk.starpasspro.api.tags.services.TagService;
import lk.starpasspro.api.tags.translators.exceptions.ResourceNotFoundException;
import rx.Observable;

@Service
public class TagServiceImpl implements TagService {

	private static final Logger log = LoggerFactory.getLogger(TagService.class);

	@Autowired
	private TagRepository tagRepository;

	@Autowired
	private TagMapper tagMapper;

	@Override
	public Observable<TagOutputDto> saveTag(TagInputDto tagDto) {
		return save(tagMapper.toTag(tagDto)).map(savedTag -> tagMapper.toTagOutputDto(savedTag));
	}

	@Override
	public Observable<TagOutputDto> updateTag(String id, TagInputDto tagDto) {
		return findOne(id).flatMap(fetchedTag -> {
			if (fetchedTag == null)
				throw new ResourceNotFoundException("No tag found for update with Id : " + id);
			BeanUtils.copyProperties(tagDto, fetchedTag, "id", "createdAt", "updatedAt");
			return save(fetchedTag).map(updatedTag -> tagMapper.toTagOutputDto(updatedTag));
		});
	}

	@Override
	public Observable<TagOutputDto> findTag(String id) {
		return findOne(id).map(fetchedTag -> {
			if (fetchedTag == null)
				throw new ResourceNotFoundException("No tag found with Id : " + id);
			return tagMapper.toTagOutputDto(fetchedTag);
		});
	}

	@Override
	public Observable<List<TagOutputDto>> findAllTagsByIds(List<String> ids) {
		return findAll(ids).map(fetchedTags -> tagMapper.toTagOutputDtos(fetchedTags));
	}

	@Override
	public Observable<List<TagOutputDto>> findAllTags() {
		return findAll().map(fetchedTags -> tagMapper.toTagOutputDtos(fetchedTags));
	}

	@Override
	public Observable<Void> deleteTag(String id) {
		return findOne(id).flatMap(fetchedTag -> {
			if (fetchedTag == null)
				throw new ResourceNotFoundException("No tag found for delete with Id : " + id);
			return delete(id);
		});
	}

	private Observable<Tag> save(Tag tag) {
		return Observable.<Tag>create(sub -> {
			Tag savedTag = tagRepository.save(tag);
			sub.onNext(savedTag);
			sub.onCompleted();
		}).doOnNext(p -> log.debug("Tag was successfully saved."))
				.doOnError(e -> log.error("An ERROR occurred while saving the Tag.", e));
	}

	private Observable<Tag> findOne(String id) {
		return Observable.<Tag>create(sub -> {
			Tag tag = tagRepository.findOne(id);
			sub.onNext(tag);
			sub.onCompleted();
		}).doOnNext(p -> log.debug("Tag was successfully fetched."))
				.doOnError(e -> log.error("An ERROR occurred while fetching the Tag.", e));
	}

	private Observable<List<Tag>> findAll(List<String> ids) {
		return Observable.<List<Tag>>create(sub -> {
			List<Tag> tags = (List<Tag>) tagRepository.findAll(ids);
			sub.onNext(tags);
			sub.onCompleted();
		}).doOnNext(p -> log.debug("Tags were successfully fetched."))
				.doOnError(e -> log.error("An ERROR occurred while fetching the Tags.", e));
	}

	private Observable<List<Tag>> findAll() {
		return Observable.<List<Tag>>create(sub -> {
			List<Tag> tags = (List<Tag>) tagRepository.findAll();
			sub.onNext(tags);
			sub.onCompleted();
		}).doOnNext(p -> log.debug("Tags were successfully fetched."))
				.doOnError(e -> log.error("An ERROR occurred while fetching the Tags.", e));
	}

	private Observable<Void> delete(String id) {
		return Observable.<Void>create(sub -> {
			tagRepository.delete(id);
			sub.onNext(null);
			sub.onCompleted();
		}).doOnNext(p -> log.debug("Tag was successfully deleted."))
				.doOnError(e -> log.error("An ERROR occurred while deleting the Tag.", e));
	}

}
