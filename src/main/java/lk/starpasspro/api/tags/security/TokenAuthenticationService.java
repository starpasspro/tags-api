package lk.starpasspro.api.tags.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lk.starpasspro.api.tags.services.AccountCredentialService;

@Service
public class TokenAuthenticationService {

	@Value("${jwt.secret}")
	private String secret;

	@Value("${jwt.token.prefix}")
	private String tokenPrefix;

	@Value("${jwt.header}")
	private String headerString;

	@Autowired
	private AccountCredentialService accountCredentialService;

	public void addAuthentication(HttpServletResponse response, String username) {
		String JWT = Jwts.builder().setSubject(username).signWith(SignatureAlgorithm.HS512, secret).compact();
		response.addHeader(headerString, tokenPrefix + " " + JWT);
	}

	public Authentication getAuthentication(HttpServletRequest request) {
		String token = request.getHeader(headerString);
		if (token != null) {
			String username;
			try {
				username = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody().getSubject();

				if (username != null) {
					return new AuthenticatedUser(accountCredentialService.getCredential(username));
				}

			} catch (Exception e) {
				return null;
			}
		}
		return null;
	}
}
