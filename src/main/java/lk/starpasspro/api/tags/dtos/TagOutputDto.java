package lk.starpasspro.api.tags.dtos;

import lk.starpasspro.api.tags.models.TagRelation;

public class TagOutputDto {

	private String id;
	private String name;
	private String owner;
	private TagRelation relatedTo;
	private String createdAt;
	private String updatedAt;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public TagRelation getRelatedTo() {
		return relatedTo;
	}
	public void setRelatedTo(TagRelation relatedTo) {
		this.relatedTo = relatedTo;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	
}
