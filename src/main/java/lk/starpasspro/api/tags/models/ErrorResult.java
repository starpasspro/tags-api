package lk.starpasspro.api.tags.models;

import java.util.List;

import org.springframework.validation.FieldError;

public class ErrorResult extends Result {
	private List<FieldError> fieldErrors;

	public List<FieldError> getFieldErrors() {
		return fieldErrors;
	}

	public void setFieldErrors(List<FieldError> fieldErrors) {
		this.fieldErrors = fieldErrors;
	}
}
