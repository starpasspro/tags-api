package lk.starpasspro.api.tags.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import lk.starpasspro.api.tags.dtos.TagInputDto;
import lk.starpasspro.api.tags.dtos.TagOutputDto;
import lk.starpasspro.api.tags.models.Tag;

@Mapper(componentModel = "spring")
public interface TagMapper {

	@Mapping(target = "id", ignore = true)
	Tag toTag(TagInputDto tagDto);

	@Mappings({ @Mapping(target = "createdAt", expression = "java(tag.getCreatedAt().toString())"),
			@Mapping(target = "updatedAt", expression = "java(tag.getUpdatedAt().toString())") })
	TagOutputDto toTagOutputDto(Tag tag);

	List<TagOutputDto> toTagOutputDtos(List<Tag> tags);

}
