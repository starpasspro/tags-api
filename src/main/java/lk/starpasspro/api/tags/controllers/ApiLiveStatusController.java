package lk.starpasspro.api.tags.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiLiveStatusController {

	@RequestMapping("/api/check/token")
	public @ResponseBody String checkToken() {
		return "authenticated :: with token";
	}
	
}
