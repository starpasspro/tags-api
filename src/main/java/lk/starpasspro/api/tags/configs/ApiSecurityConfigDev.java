package lk.starpasspro.api.tags.configs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import lk.starpasspro.api.tags.services.SecureUserDetailsService;

@Configuration
@Profile("dev")
public class ApiSecurityConfigDev extends WebSecurityConfigurerAdapter {

	@Autowired
	private SecureUserDetailsService secureUserDetailsService;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.headers().cacheControl();
		http.csrf().disable().authorizeRequests().antMatchers("/").permitAll();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(secureUserDetailsService);
	}

}
