package lk.starpasspro.api.tags.services;

import java.util.List;

import lk.starpasspro.api.tags.dtos.TagInputDto;
import lk.starpasspro.api.tags.dtos.TagOutputDto;
import rx.Observable;

public interface TagService {
	Observable<TagOutputDto> saveTag(TagInputDto tagDto);

	Observable<TagOutputDto> updateTag(String id, TagInputDto tagDto);

	Observable<TagOutputDto> findTag(String id);

	Observable<List<TagOutputDto>> findAllTagsByIds(List<String> ids);

	Observable<List<TagOutputDto>> findAllTags();

	Observable<Void> deleteTag(String id);
}
