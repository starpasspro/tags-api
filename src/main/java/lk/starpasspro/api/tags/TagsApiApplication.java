package lk.starpasspro.api.tags;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication
@EnableAutoConfiguration
@EnableMongoAuditing
@EnableWebSecurity
@EnableDiscoveryClient
public class TagsApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TagsApiApplication.class, args);
	}
}
