package lk.starpasspro.api.tags.models;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum TagRelation {
	EXAMS,QUESTIONS,RESOURCES,PROFILES,ALL;
	
	private static Map<String, TagRelation> RELATIONS_MAP = Stream.of(TagRelation.values())
			.collect(Collectors.toMap(s -> s.name(), Function.identity()));

	public static TagRelation fromString(String key) {
		TagRelation relation = RELATIONS_MAP.get(key);

		if (relation == null) {
			throw new IllegalArgumentException("\'" + key + "\' invalid relation value");
		}
		return relation;
	}
}
