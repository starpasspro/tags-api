package lk.starpasspro.api.tags.models;

public class Result {

	protected ResultType type;
	protected String msg;

	public ResultType getType() {
		return type;
	}

	public void setType(ResultType type) {
		this.type = type;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
