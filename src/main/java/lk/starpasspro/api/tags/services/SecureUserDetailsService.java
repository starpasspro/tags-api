package lk.starpasspro.api.tags.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import lk.starpasspro.api.tags.models.AccountCredential;
import lk.starpasspro.api.tags.security.SecureUserDetails;

@Service
public class SecureUserDetailsService implements UserDetailsService {

	@Autowired
	AccountCredentialService userCredentialService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		AccountCredential userCredential = userCredentialService.getCredential(username);
		if (userCredential == null) {
			throw new UsernameNotFoundException(username);
		} else {
			UserDetails details = new SecureUserDetails(userCredential);
			return details;
		}
	}

}