package lk.starpasspro.api.tags.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lk.starpasspro.api.tags.models.AccountCredential;
import lk.starpasspro.api.tags.repositories.AccountCredentialsRepository;
import lk.starpasspro.api.tags.services.AccountCredentialService;

@Service
public class AccountCredentialServiceImpl implements AccountCredentialService {

	@Autowired
	private AccountCredentialsRepository accountCredentialsRepository;

	@Override
	public AccountCredential getCredential(String username) {
		return accountCredentialsRepository.findByUsername(username);
	}

}
