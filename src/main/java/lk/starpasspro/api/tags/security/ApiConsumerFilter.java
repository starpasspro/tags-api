package lk.starpasspro.api.tags.security;

import com.fasterxml.jackson.databind.ObjectMapper;

import lk.starpasspro.api.tags.models.AccountCredential;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ApiConsumerFilter extends AbstractAuthenticationProcessingFilter {

	private TokenAuthenticationService tokenAuthenticationService;

	public ApiConsumerFilter(String url, AuthenticationManager authenticationManager,
			TokenAuthenticationService _tokenAuthenticationService) {
		super(new AntPathRequestMatcher(url));
		setAuthenticationManager(authenticationManager);
		tokenAuthenticationService = _tokenAuthenticationService;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse) throws AuthenticationException, IOException, ServletException {
		AccountCredential credentials = new ObjectMapper().readValue(httpServletRequest.getInputStream(),
				AccountCredential.class);
		UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(credentials.getUsername(),
				credentials.getPassword());
		return getAuthenticationManager().authenticate(token);
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authentication) throws IOException, ServletException {
		String name = authentication.getName();
		tokenAuthenticationService.addAuthentication(response, name);
	}
}
