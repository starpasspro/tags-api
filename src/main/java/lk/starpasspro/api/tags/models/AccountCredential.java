package lk.starpasspro.api.tags.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "account_credentials")
public class AccountCredential {

	@Id
	private String id;
	private String username;
	private String password;
	private String userRole = "API_CONSUMER";

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getId() {
		return id;
	}

	public void setId(String _id) {
		this.id = _id;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String _userRole) {
		this.userRole = _userRole;
	}

	public void setUsername(String _username) {
		this.username = _username;
	}

	public void setPassword(String _password) {
		this.password = _password;
	}
}
