package lk.starpasspro.api.tags.security;

import java.util.Collection;
import java.util.LinkedList;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import lk.starpasspro.api.tags.models.AccountCredential;

public class AuthenticatedUser implements Authentication {

	private static final long serialVersionUID = 1L;
	private AccountCredential credential;
	private boolean authenticated = true;

	AuthenticatedUser(AccountCredential credential) {
		this.credential = credential;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(credential.getUserRole());
		Collection<GrantedAuthority> grantedAuthorities = new LinkedList<GrantedAuthority>();
		grantedAuthorities.add(grantedAuthority);
		return grantedAuthorities;
	}

	@Override
	public Object getCredentials() {
		return credential.getPassword();
	}

	@Override
	public Object getDetails() {
		return null;
	}

	@Override
	public Object getPrincipal() {
		return credential.getUsername();
	}

	@Override
	public boolean isAuthenticated() {
		return this.authenticated;
	}

	@Override
	public void setAuthenticated(boolean b) throws IllegalArgumentException {
		this.authenticated = b;
	}

	@Override
	public String getName() {
		return credential.getUsername();
	}
}
