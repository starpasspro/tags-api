package lk.starpasspro.api.tags;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;

import org.apache.http.HttpStatus;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lk.starpasspro.api.tags.dtos.TagInputDto;
import lk.starpasspro.api.tags.dtos.TagOutputDto;
import lk.starpasspro.api.tags.models.AccountCredential;
import lk.starpasspro.api.tags.models.TagRelation;
import lk.starpasspro.api.tags.repositories.AccountCredentialsRepository;
import lk.starpasspro.api.tags.repositories.TagRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TagsApiApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class TagsApiITests {

	@Value("${local.server.port}")
	private int port;

	@Value("${jwt.secret}")
	private String secret;

	@Value("${jwt.token.prefix}")
	private String tokenPrefix;

	@Value("${jwt.header}")
	private String headerString;

	@Autowired
	private AccountCredentialsRepository accountCredentialsRepository;

	@Autowired
	private TagRepository tagRepository;

	private String securityToken;
	private TagInputDto tagInput;

	@Before
	public void setUp() {
		RestAssured.port = port;

		accountCredentialsRepository.deleteAll();

		AccountCredential accountCredential = new AccountCredential();
		accountCredential.setUsername("test_user");
		accountCredential.setPassword("test_user");

		accountCredentialsRepository.save(accountCredential);

		securityToken = Jwts.builder().setSubject("test_user").signWith(SignatureAlgorithm.HS512, secret).compact();

		tagInput = createTagInput();
	}

	@After
	public void cleanUp() {
		accountCredentialsRepository.deleteAll();
		tagRepository.deleteAll();
	}

	/*
	 *
	 * Starts Save Tag Related Tests
	 *
	 */
	@Test
	public void testSaveTag() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(tagInput)
				.post("/api/tags/").then().statusCode(HttpStatus.SC_OK).body("id", notNullValue())
				.body("name", is(tagInput.getName())).body("owner", is(tagInput.getOwner()))
				.body("relatedTo", is(tagInput.getRelatedTo().name())).body("createdAt", notNullValue())
				.body("updatedAt", notNullValue());
	}

	@Test
	public void testSaveTagWithNullName() {
		tagInput.setName(null);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(tagInput)
				.post("/api/tags/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveTagWithEmptyName() {
		tagInput.setName("");
		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(tagInput)
				.post("/api/tags/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveTagWithNullOwner() {
		tagInput.setOwner(null);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(tagInput)
				.post("/api/tags/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveTagWithEmptyOwner() {
		tagInput.setOwner("");
		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(tagInput)
				.post("/api/tags/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveTagWithNullTag() {
		tagInput.setRelatedTo(null);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(tagInput)
				.post("/api/tags/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveTagWithInvalidTag() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content("{\"name\":\"Tag 01\",\"owner\":\"StarPassPro\",\"relatedTo\":\"AL\"}").post("/api/tags/")
				.then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	/*
	 *
	 * Ends Save Tag Related Tests
	 *
	 */

	/*
	 *
	 * Starts Update Tag Related Tests
	 *
	 */
	@Test
	public void testUpdateTag() {
		TagOutputDto tagOutput = given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(tagInput).post("/api/tags/").then().statusCode(HttpStatus.SC_OK)
				.body("id", notNullValue()).body("name", is(tagInput.getName())).body("owner", is(tagInput.getOwner()))
				.body("relatedTo", is(tagInput.getRelatedTo().name())).body("createdAt", notNullValue())
				.body("updatedAt", notNullValue()).extract().as(TagOutputDto.class);

		tagInput.setName("Tag 01 Updated");
		tagInput.setRelatedTo(TagRelation.PROFILES);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(tagInput)
				.put("/api/tags/" + tagOutput.getId()).then().statusCode(HttpStatus.SC_OK)
				.body("id", is(tagOutput.getId())).body("name", is(tagInput.getName()))
				.body("owner", is(tagOutput.getOwner())).body("relatedTo", is(tagInput.getRelatedTo().name()))
				.body("createdAt", is(tagOutput.getCreatedAt())).body("updatedAt", not(tagOutput.getUpdatedAt()));
	}

	@Test
	public void testUpdateTagWithNullName() {
		TagOutputDto tagOutput = given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(tagInput).post("/api/tags/").then().statusCode(HttpStatus.SC_OK).extract()
				.as(TagOutputDto.class);
		tagInput.setName(null);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(tagInput)
				.put("/api/tags/" + tagOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateTagWithEmptyName() {
		TagOutputDto tagOutput = given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(tagInput).post("/api/tags/").then().statusCode(HttpStatus.SC_OK).extract()
				.as(TagOutputDto.class);
		tagInput.setName("");
		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(tagInput)
				.put("/api/tags/" + tagOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateTagWithNullOwner() {
		TagOutputDto tagOutput = given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(tagInput).post("/api/tags/").then().statusCode(HttpStatus.SC_OK).extract()
				.as(TagOutputDto.class);
		tagInput.setOwner(null);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(tagInput)
				.put("/api/tags/" + tagOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateTagWithEmptyOwner() {
		TagOutputDto tagOutput = given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(tagInput).post("/api/tags/").then().statusCode(HttpStatus.SC_OK).extract()
				.as(TagOutputDto.class);
		tagInput.setOwner("");
		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(tagInput)
				.put("/api/tags/" + tagOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateTagWithNullTag() {
		TagOutputDto tagOutput = given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(tagInput).post("/api/tags/").then().statusCode(HttpStatus.SC_OK).extract()
				.as(TagOutputDto.class);
		tagInput.setRelatedTo(null);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(tagInput)
				.put("/api/tags/" + tagOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateTagWithInvalidTag() {
		TagOutputDto tagOutput = given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(tagInput).post("/api/tags/").then().statusCode(HttpStatus.SC_OK).extract()
				.as(TagOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content("{\"name\":\"Tag 01\",\"owner\":\"StarPassPro\",\"relatedTo\":\"AL\"}")
				.put("/api/tags/" + tagOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	/*
	 *
	 * Ends Update Tag Related Tests
	 *
	 */

	/*
	 *
	 * Starts Get Tag by ID Tests
	 *
	 */
	@Test
	public void testGetTag() {
		TagOutputDto tagOutput = given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(tagInput).post("/api/tags/").then().statusCode(HttpStatus.SC_OK).extract()
				.as(TagOutputDto.class);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.get("/api/tags/" + tagOutput.getId()).then().statusCode(HttpStatus.SC_OK)
				.body("id", is(tagOutput.getId())).body("name", is(tagOutput.getName()))
				.body("owner", is(tagOutput.getOwner())).body("relatedTo", is(tagOutput.getRelatedTo().name()))
				.body("createdAt", is(tagOutput.getCreatedAt())).body("updatedAt", is(tagOutput.getUpdatedAt()));
	}

	@Test
	public void testGetTagWithNullId() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when().get("/api/tags/null").then()
				.statusCode(HttpStatus.SC_NOT_FOUND);
	}

	@Test
	public void testGetTagWithNonExistingId() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when().get("/api/tags/abc").then()
				.statusCode(HttpStatus.SC_NOT_FOUND);
	}

	/*
	 *
	 * Ends Get Tag by ID Tests
	 *
	 */

	/*
	 *
	 * Starts Get All Tags Tests
	 *
	 */
	@Test
	public void testGetAllTags() {
		TagOutputDto tag1 = given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(tagInput).post("/api/tags/").then().statusCode(HttpStatus.SC_OK).extract()
				.as(TagOutputDto.class);

		TagOutputDto tag2 = given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(tagInput).post("/api/tags/").then().statusCode(HttpStatus.SC_OK).extract()
				.as(TagOutputDto.class);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().get("/api/tags/all").then()
				.statusCode(HttpStatus.SC_OK).body("$", Matchers.hasSize(2))
				.body("id", hasItems(tag1.getId(), tag2.getId())).body("name", hasItems(tag1.getName(), tag2.getName()))
				.body("owner", hasItems(tag1.getOwner(), tag2.getOwner()))
				.body("relatedTo", hasItems(tag1.getRelatedTo().name(), tag2.getRelatedTo().name()))
				.body("createdAt", hasItems(tag1.getCreatedAt(), tag2.getCreatedAt()))
				.body("updatedAt", hasItems(tag1.getUpdatedAt(), tag2.getUpdatedAt()));
	}

	@Test
	public void testGetTagsWithEmptyDB() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when().get("/api/tags/all").then()
				.statusCode(HttpStatus.SC_OK).body("$", Matchers.hasSize(0));
	}

	/*
	 *
	 * Ends Get All Tags Tests
	 *
	 */

	/*
	 *
	 * Starts Get Tags by IDs Tests
	 *
	 */
	@Test
	public void testGetTagsByIds() {
		TagOutputDto tag1 = given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(tagInput).post("/api/tags/").then().statusCode(HttpStatus.SC_OK).extract()
				.as(TagOutputDto.class);

		TagOutputDto tag2 = given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(tagInput).post("/api/tags/").then().statusCode(HttpStatus.SC_OK).extract()
				.as(TagOutputDto.class);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(tagInput)
				.post("/api/tags/").then().statusCode(HttpStatus.SC_OK);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.get("/api/tags/all?id=" + tag1.getId() + "&id=" + tag2.getId()).then()
				.statusCode(HttpStatus.SC_OK).body("$", Matchers.hasSize(2))
				.body("id", hasItems(tag1.getId(), tag2.getId())).body("name", hasItems(tag1.getName(), tag2.getName()))
				.body("owner", hasItems(tag1.getOwner(), tag2.getOwner()))
				.body("relatedTo", hasItems(tag1.getRelatedTo().name(), tag2.getRelatedTo().name()))
				.body("createdAt", hasItems(tag1.getCreatedAt(), tag2.getCreatedAt()))
				.body("updatedAt", hasItems(tag1.getUpdatedAt(), tag2.getUpdatedAt()));
	}

	@Test
	public void testGetTagsByIdsWithNullId() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when().get("/api/tags/all?id=null")
				.then().statusCode(HttpStatus.SC_OK).body("$", Matchers.hasSize(0));
	}

	@Test
	public void testGetResourcesByIdsWithNonExistingId() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when().get("/api/tags/all?id=abc")
				.then().statusCode(HttpStatus.SC_OK).body("$", Matchers.hasSize(0));
	}

	@Test
	public void testGetTagsByIdsWithOneNullId() {
		TagOutputDto tag1 = given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(tagInput).post("/api/tags/").then().statusCode(HttpStatus.SC_OK).extract()
				.as(TagOutputDto.class);

		TagOutputDto tag2 = given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(tagInput).post("/api/tags/").then().statusCode(HttpStatus.SC_OK).extract()
				.as(TagOutputDto.class);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(tagInput)
				.post("/api/tags/").then().statusCode(HttpStatus.SC_OK);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.get("/api/tags/all?id=" + tag1.getId() + "&id=" + tag2.getId() + "&id=null").then()
				.statusCode(HttpStatus.SC_OK).body("$", Matchers.hasSize(2))
				.body("id", hasItems(tag1.getId(), tag2.getId())).body("name", hasItems(tag1.getName(), tag2.getName()))
				.body("owner", hasItems(tag1.getOwner(), tag2.getOwner()))
				.body("relatedTo", hasItems(tag1.getRelatedTo().name(), tag2.getRelatedTo().name()))
				.body("createdAt", hasItems(tag1.getCreatedAt(), tag2.getCreatedAt()))
				.body("updatedAt", hasItems(tag1.getUpdatedAt(), tag2.getUpdatedAt()));
	}

	@Test
	public void testGetTagsByIdsWithOneNonExisting() {
		TagOutputDto tag1 = given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(tagInput).post("/api/tags/").then().statusCode(HttpStatus.SC_OK).extract()
				.as(TagOutputDto.class);

		TagOutputDto tag2 = given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(tagInput).post("/api/tags/").then().statusCode(HttpStatus.SC_OK).extract()
				.as(TagOutputDto.class);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(tagInput)
				.post("/api/tags/").then().statusCode(HttpStatus.SC_OK);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.get("/api/tags/all?id=" + tag1.getId() + "&id=" + tag2.getId() + "&id=abc").then()
				.statusCode(HttpStatus.SC_OK).body("$", Matchers.hasSize(2))
				.body("id", hasItems(tag1.getId(), tag2.getId())).body("name", hasItems(tag1.getName(), tag2.getName()))
				.body("owner", hasItems(tag1.getOwner(), tag2.getOwner()))
				.body("relatedTo", hasItems(tag1.getRelatedTo().name(), tag2.getRelatedTo().name()))
				.body("createdAt", hasItems(tag1.getCreatedAt(), tag2.getCreatedAt()))
				.body("updatedAt", hasItems(tag1.getUpdatedAt(), tag2.getUpdatedAt()));
	}

	/*
	 *
	 * Ends Get Tags by IDs Tests
	 *
	 */

	/*
	 *
	 * Starts Delete tag Tests
	 *
	 */
	@Test
	public void testDeleteTag() {
		TagOutputDto tagOutput = given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(tagInput).post("/api/tags/").then().statusCode(HttpStatus.SC_OK).extract()
				.as(TagOutputDto.class);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.delete("/api/tags/" + tagOutput.getId()).then().statusCode(HttpStatus.SC_OK);

		Assert.assertEquals(0, tagRepository.count());
	}

	@Test
	public void testDeleteTagWithNullId() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when().delete("/api/tags/null").then()
				.statusCode(HttpStatus.SC_NOT_FOUND);
	}

	@Test
	public void testDeleteTagWithNonExistingId() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when().delete("/api/tags/abc").then()
				.statusCode(HttpStatus.SC_NOT_FOUND);
	}

	/*
	 *
	 * Ends Delete Tag Tests
	 *
	 */

	private TagInputDto createTagInput() {
		TagInputDto tagInputDto = new TagInputDto();
		tagInputDto.setName("Tag 01");
		tagInputDto.setOwner("StarPassPro");
		tagInputDto.setRelatedTo(TagRelation.ALL);

		return tagInputDto;
	}

}
