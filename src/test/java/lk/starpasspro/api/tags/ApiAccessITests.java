package lk.starpasspro.api.tags;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;

import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lk.starpasspro.api.tags.models.AccountCredential;
import lk.starpasspro.api.tags.repositories.AccountCredentialsRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TagsApiApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class ApiAccessITests {

	@Autowired
	private AccountCredentialsRepository accountCredentialsRepository;

	@Value("${local.server.port}")
	private int port;

	@Value("${jwt.secret}")
	private String secret;

	@Value("${jwt.token.prefix}")
	private String tokenPrefix;

	@Value("${jwt.header}")
	private String headerString;

	@Before
	public void setUp() {
		RestAssured.port = port;
		accountCredentialsRepository.deleteAll();
	}

	@Test
	public void testAccessResourceWithoutAuthorization() {
		given().contentType(ContentType.JSON).when().get("/api/check/token").then().statusCode(HttpStatus.SC_FORBIDDEN);
	}

	@Test
	public void testApiConsumerTokenGenerate() {

		AccountCredential accountCredential = new AccountCredential();
		accountCredential.setUsername("test_user");
		accountCredential.setPassword("test_user");

		accountCredentialsRepository.save(accountCredential);

		String expectedToken = Jwts.builder().setSubject("test_user").signWith(SignatureAlgorithm.HS512, secret)
				.compact();

		given().contentType(ContentType.JSON).when()
				.content("{\"username\" : \"test_user\", \"password\" : \"test_user\"}").post("/api/consumer").then()
				.statusCode(HttpStatus.SC_OK).header(headerString, tokenPrefix + " " + expectedToken);

	}

	@Test
	public void testApiConsumerTokenGenerateForNonExistingUser() {

		given().contentType(ContentType.JSON).when()
				.content("{\"username\" : \"test_user\", \"password\" : \"test_user\"}").post("/api/consumer").then()
				.statusCode(HttpStatus.SC_UNAUTHORIZED);

	}

	@Test
	public void testApiAccessForGeneratedToken() {

		AccountCredential accountCredential = new AccountCredential();
		accountCredential.setUsername("test_user");
		accountCredential.setPassword("test_user");

		accountCredentialsRepository.save(accountCredential);

		String generatedToken = Jwts.builder().setSubject("test_user").signWith(SignatureAlgorithm.HS512, secret)
				.compact();

		given().contentType(ContentType.JSON).header(headerString, generatedToken).when().get("/api/check/token").then()
				.statusCode(HttpStatus.SC_OK).body(containsString("authenticated :: with token"));

	}
	
	@Test
	public void testApiAccessForInvalidGeneratedToken() {

		AccountCredential accountCredential = new AccountCredential();
		accountCredential.setUsername("test_user");
		accountCredential.setPassword("test_user");

		accountCredentialsRepository.save(accountCredential);

		given().contentType(ContentType.JSON).header(headerString, "abc").when().get("/api/check/token").then()
				.statusCode(HttpStatus.SC_FORBIDDEN);

	}

}
