package lk.starpasspro.api.tags.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import lk.starpasspro.api.tags.models.Tag;

public interface TagRepository extends MongoRepository<Tag, String> {

}
