package lk.starpasspro.api.tags.models;

public class SuccessResult extends Result {
	private Object data;

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
