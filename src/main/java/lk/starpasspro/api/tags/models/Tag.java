package lk.starpasspro.api.tags.models;

import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "tags")
public class Tag {

	@Id
	private String id;
	private String name;
	private String owner;
	private TagRelation relatedTo;
	
	@CreatedDate
	private DateTime createdAt;

	@LastModifiedDate
	private DateTime updatedAt;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public TagRelation getRelatedTo() {
		return relatedTo;
	}

	public void setRelatedTo(TagRelation relatedTo) {
		this.relatedTo = relatedTo;
	}
	
	public DateTime getCreatedAt() {
		return createdAt;
	}

	public DateTime getUpdatedAt() {
		return updatedAt;
	}

}
