package lk.starpasspro.api.tags.dtos;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import lk.starpasspro.api.tags.models.TagRelation;

public class TagInputDto {

	@NotNull(message = "'name' Cannot be null")
	@NotEmpty(message = "'name' Cannot be empty")
	private String name;

	@NotNull(message = "'owner' Cannot be null")
	@NotEmpty(message = "'owner' Cannot be empty")
	private String owner;

	@NotNull(message = "'relatedTo' Cannot be null")
	private TagRelation relatedTo;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public TagRelation getRelatedTo() {
		return relatedTo;
	}

	public void setRelatedTo(TagRelation relatedTo) {
		this.relatedTo = relatedTo;
	}

}
