package lk.starpasspro.api.tags.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import lk.starpasspro.api.tags.models.AccountCredential;

public interface AccountCredentialsRepository extends MongoRepository<AccountCredential, String> {
	@Query("{'username':?0}")
	AccountCredential findByUsername(String username);
}
