package lk.starpasspro.api.tags.controllers;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lk.starpasspro.api.tags.dtos.TagInputDto;
import lk.starpasspro.api.tags.dtos.TagOutputDto;
import lk.starpasspro.api.tags.services.TagService;
import rx.Observable;

@RestController
@RequestMapping("/api/tags")
public class TagController {

	private static Logger logger = LoggerFactory.getLogger(TagController.class);

	@Autowired
	private TagService tagService;

	@RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Observable<TagOutputDto> saveTag(@Validated @RequestBody TagInputDto tagDto) {
		logger.info("Request to save Tag {} " + tagDto);
		return tagService.saveTag(tagDto);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public Observable<TagOutputDto> updateTag(@PathVariable String id, @Validated @RequestBody TagInputDto tagDto) {
		return tagService.updateTag(id, tagDto);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Observable<TagOutputDto> findTag(@PathVariable String id) {
		return tagService.findTag(id);
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Observable<List<TagOutputDto>> findAllTagByIds(@RequestParam(name = "id", required = false) String[] ids) {
		if (ids != null && ids.length > 0)
			return tagService.findAllTagsByIds(Arrays.asList(ids));
		return tagService.findAllTags();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Observable<Void> deleteTag(@PathVariable String id) {
		return tagService.deleteTag(id);
	}

}
