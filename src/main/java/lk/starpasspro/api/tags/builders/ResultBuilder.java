package lk.starpasspro.api.tags.builders;

import java.util.List;

import org.springframework.validation.FieldError;

import lk.starpasspro.api.tags.models.ErrorResult;
import lk.starpasspro.api.tags.models.ResultType;
import lk.starpasspro.api.tags.models.SuccessResult;

public class ResultBuilder {

	public static ErrorResult buildErrorResult(String msg, List<FieldError> fieldErrors) {
		ErrorResult errorResult = new ErrorResult();
		errorResult.setFieldErrors(fieldErrors);
		errorResult.setMsg(msg);
		errorResult.setType(ResultType.ERROR);

		return errorResult;
	}

	public static SuccessResult buildSuccessResult(String msg, Object data) {
		SuccessResult successResult = new SuccessResult();
		successResult.setMsg(msg);
		successResult.setType(ResultType.SUCCESS);
		successResult.setData(data);

		return successResult;
	}

}
