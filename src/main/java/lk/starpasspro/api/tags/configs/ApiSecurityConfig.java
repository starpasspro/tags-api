package lk.starpasspro.api.tags.configs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import lk.starpasspro.api.tags.security.ApiAuthenticationFilter;
import lk.starpasspro.api.tags.security.ApiConsumerFilter;
import lk.starpasspro.api.tags.security.TokenAuthenticationService;
import lk.starpasspro.api.tags.services.SecureUserDetailsService;

@Configuration
@Profile("!dev")
public class ApiSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private TokenAuthenticationService tokenAuthenticationService;

	@Autowired
	SecureUserDetailsService secureUserDetailsService;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.headers().cacheControl();
		http.csrf().disable().authorizeRequests().antMatchers("/health", "/info").permitAll()
				.antMatchers(HttpMethod.POST, "/api/consumer").permitAll().anyRequest().authenticated().and()
				.addFilterBefore(
						new ApiConsumerFilter("/api/consumer", authenticationManager(), tokenAuthenticationService),
						UsernamePasswordAuthenticationFilter.class)
				.addFilterBefore(new ApiAuthenticationFilter(tokenAuthenticationService),
						UsernamePasswordAuthenticationFilter.class);
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(secureUserDetailsService);
	}

}
