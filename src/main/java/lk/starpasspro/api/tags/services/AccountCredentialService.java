package lk.starpasspro.api.tags.services;

import lk.starpasspro.api.tags.models.AccountCredential;

public interface AccountCredentialService {
	AccountCredential getCredential(String username);
}
