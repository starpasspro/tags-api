package lk.starpasspro.api.tags.utils;

public class ErrorMessage {
	public static String VALIDATION_ERR_MSG = "Error : Validation";
	public static String ACCESS_DENIED_ERR_MSG = "Error : Access Denied";
	public static String METHOD_NOT_ALLOWED_ERR_MSG = "Error : Method Not Allowed";
	public static String INTERNAL_SERVER_ERR_MSG = "Internal Server Error";
}
