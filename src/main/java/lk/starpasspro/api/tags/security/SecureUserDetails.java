package lk.starpasspro.api.tags.security;

import java.util.Collection;
import java.util.LinkedList;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lk.starpasspro.api.tags.models.AccountCredential;

public class SecureUserDetails implements UserDetails {

	private static final long serialVersionUID = 1L;
	private AccountCredential userCredential;

	public SecureUserDetails(AccountCredential userCredential) {
		this.userCredential = userCredential;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(userCredential.getUserRole());
		Collection<GrantedAuthority> grantedAuthorities = new LinkedList<GrantedAuthority>();
		grantedAuthorities.add(grantedAuthority);
		return grantedAuthorities;
	}

	@Override
	public String getPassword() {
		return userCredential.getPassword();
	}

	@Override
	public String getUsername() {
		return userCredential.getUsername();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}
